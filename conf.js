const specs = require('./specs')

exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  specs: specs.map((s) => 'specs/' + s)
};