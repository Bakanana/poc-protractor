const todosSpecs = require('./todos')

const specs = [
    ...todosSpecs.map((spec) => 'todos/' + spec)
]

module.exports = specs
